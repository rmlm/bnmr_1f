# `bnmr_1f`

`bnmr_1f` is a command line tool to process 1f mode (i.e., CW RF) β-NMR data taken at [TRIUMF]'s ISAC facility.
It was developed to address some of the limitations present in existing analysis tools.
While it does not aim to be all-purpose, it attempts to provide a (relatively) painless means of inspecting/processing/exporting data. 
Notable features include:

- All measured asymmetries, even those from incomplete scans, are used to construct the combined asymmetry (i.e., weighted average from all scans).
- If a scan has a glitch, rather than throw away the whole scan, you can explicitly specify the bad bins to exclude.
- If there are several runs taken under identical conditions, rather than analyze them separately, you can combine them into a single spectra - as if they were taken a single long run. The combined scans need not have identical frequency steps or ranges, which is especially useful when several runs are used to identify fine/broad features in a particular resonance. It is up to the user to decide how appropriate it is to combine a particular set of runs.

### Requirements

- A [C++14] compiler (e.g., [GCC] v5 or greater).
- The [MUD] library.
- The [ROOT] library (preferably v6).

### Building

For the sake of brevity, it is assumed that you *already* have [ROOT] installed on your system. The [MUD] library can be obtained and installed, for example, as follows:

```bash
$ wget http://cmms.triumf.ca/mud/mud.zip
$ unzip mud.zip
$ cd mud/
$ make all
$ sudo make install
$ sudo cp src/mud.h /usr/local/include
```
 Now onto `bnmr_1f`! From your terminal simply execute:

```bash
$ git clone https://gitlab.com/rmlm/bnmr_1f.git
$ cd bnmr_1f/
$ make
```

### Use

`bnmr_1f` directly reads the [MUD] files, so it is expecting to find them somewhere on your system.
The "standard" data directory location/structure is already specified for the "usual" systems, but you may wish to add your own (see lines 31-48 in `src/bnmr_1f.cpp`).
You may wish to peruse the [web database](http://cmms.triumf.ca/mud/runSel.html) if you don't already have the data on your system.
`bnmr_1f` takes pairs of space separated command line arguments, where the first argument is the run year and the following argument is the run number.
There is no limit to the number of argument pairs that can be given (i.e., an infinite number of runs can be combined)!
For example usage, see:

```bash
$ ./bnmr_1f --help
```

[C++14]: https://en.wikipedia.org/wiki/C%2B%2B14 "C++14"
[GCC]: https://gcc.gnu.org/ "GCC | The GNU Compiler Collection"
[MUD]: http://cmms.triumf.ca/mud/ "MUon Data Format"
[ROOT]: https://root.cern.ch/ "ROOT | A Data Analysis Framework"
[TRIUMF]: http://www.triumf.ca/ "TRIUMF | Canada's National Laboratory for Particle and Nuclear Physics"

