// https://stackoverflow.com/a/18764531

#ifndef PARSE_RANGE_H
#define PARSE_RANGE_H

#include <string>
#include <vector>

namespace pr
{
   int ConvertString2Int(const std::string& str);
   std::vector<std::string> SplitStringToArray(const std::string& str, char splitter);
   std::vector<int> ParseData(const std::string& data);
} // namespace pr
#endif // PARSE_RANGE_H
