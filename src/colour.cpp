#include "colour.h"

const std::string colour::black("\033[0;30m");
const std::string colour::red("\033[0;31m");
const std::string colour::green("\033[1;32m");
const std::string colour::yellow("\033[1;33m");
const std::string colour::blue("\033[1;34m");
const std::string colour::magenta("\033[0;35m");
const std::string colour::cyan("\033[0;36m");
const std::string colour::white("\033[0;37m");
const std::string colour::reset("\033[0m");
