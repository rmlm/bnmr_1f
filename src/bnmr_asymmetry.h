#ifndef BNMR_ASYMMETRY_H
#define BNMR_ASYMMETRY_H

namespace bnmr
{
   class asymmetry
   {
   public :
      // calculate asymmetry & uncertainty for the usual two-counter method
      static double calculate(unsigned int backward_counts, unsigned int forward_counts);
      static double calculate_uncertainty(unsigned int backward_counts, unsigned int forward_counts);
      
      // calculate asymmetry & uncertainty for the four-counter method (lcrplot style)
      static double calculate(unsigned int backward_p_counts, unsigned int forward_p_counts, unsigned int backward_m_counts, unsigned int forward_m_counts);
      static double calculate_uncertainty(unsigned int backward_p_counts, unsigned int forward_p_counts, unsigned int backward_m_counts, unsigned int forward_m_counts);
   protected :
   private :
   }; // class asymmetry
} // namespace bnmr
#endif // BNMR_ASYMMETRY_H
