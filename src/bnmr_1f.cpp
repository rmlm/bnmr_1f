#include <algorithm>
#include <cmath>
#include <cstring>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <sstream>
#include <unistd.h>

#include <mud.h>

#include "bnmr_1f.h"
#include "bnmr_asymmetry.h"
#include "bnmr_statistics.h"
#include "colour.h"
#include "parse_range.h"

triumf::bnmr_1f::bnmr_1f(unsigned int the_year, unsigned int the_run) : file_type(0), number_of_logged_variables(0), number_of_histograms(0), run_year(the_year), run_number(the_run), run_experiment_number(0), run_elapsed_time(0), run_start_time(0), run_stop_time(0)
{
   // get the hostname
   char hostname_char[strdim];
   gethostname(hostname_char, strdim);
   std::string hostname(hostname_char);
   std::string parent_directory;
   std::string spectrometer_directory;
   
   if (run_number >= 40000 and run_number <= 44999) spectrometer_directory = "bnmr/";
   if (run_number >= 45000 and run_number <= 49999) spectrometer_directory = "bnqr/";
   
   // data structure on local machine
   if (hostname == "voldemort")
   {
      parent_directory.append("/home/rmlm/Documents/triumf/data/");
      file_name = parent_directory + spectrometer_directory + std::to_string(run_year) + "/0" + std::to_string(run_number) + ".msr";
   }
   if (hostname == "gandalf")
   {
      parent_directory.append("/home/rmlm/triumf/data/");
      file_name = parent_directory + spectrometer_directory + std::to_string(run_year) + "/0" + std::to_string(run_number) + ".msr";
   }
   // musr data archive folder structure
   if (hostname == "lincmms" or hostname == "lincmms.triumf.ca" or hostname == "muesli"  or hostname == "muesli.triumf.ca") 
   {
      parent_directory.append("/data/");
      file_name = parent_directory + spectrometer_directory + std::to_string(run_year) + "/0" + std::to_string(run_number) + ".msr";
   }
   // isdaq data folder structure
   if (hostname == "linbnmr2" or hostname == "linbnmr2.triumf.ca")
   {
      parent_directory.append("/data1/");
      file_name = parent_directory + spectrometer_directory + "dlog/" + std::to_string(run_year) + "/0" + std::to_string(run_number) + ".msr";
   }
   //std::cout << hostname << std::endl;
   file_handle = MUD_openRead(const_cast<char*>(file_name.c_str()), &file_type);
   
   if (file_handle == 0)
   {
      // Read the run description (i.e., title, experimenters, sample, etc...)
      unsigned int begin_time;
      unsigned int end_time;
      MUD_getRunDesc(file_handle, &file_type);
      MUD_getExptNumber(file_handle, &run_experiment_number);
      MUD_getRunNumber(file_handle, &run_number);
      MUD_getElapsedSec(file_handle, &run_elapsed_time);
      MUD_getTimeBegin(file_handle, &begin_time);
      MUD_getTimeEnd(file_handle, &end_time);
      
      run_start_time = begin_time;
      run_stop_time = end_time;
      
      char title_char[strdim];
      char lab_char[strdim];
      char area_char[strdim];
      char method_char[strdim];
      char apparatus_char[strdim];
      char insert_char[strdim];
      char sample_char[strdim];
      char orientation_char[strdim];
      char das_char[strdim];
      char experimenter_char[strdim];
      
      MUD_getTitle(file_handle, title_char, strdim);
      MUD_getLab(file_handle, lab_char, strdim);
      MUD_getArea(file_handle, area_char, strdim);
      MUD_getMethod(file_handle, method_char, strdim);
      MUD_getApparatus(file_handle, apparatus_char, strdim);
      MUD_getInsert(file_handle, insert_char, strdim);
      MUD_getSample(file_handle, sample_char, strdim);
      MUD_getOrient(file_handle, orientation_char, strdim);
      MUD_getDas(file_handle, das_char, strdim);
      MUD_getExperimenter(file_handle, experimenter_char, strdim);
      
      run_title.assign(title_char);
      run_lab.assign(lab_char);
      run_area.assign(area_char);
      run_method.assign(method_char);
      run_apparatus.assign(apparatus_char);
      run_mode.assign(insert_char);
      run_sample.assign(sample_char);
      run_sample_orientation.assign(orientation_char);
      run_data_acquisition_system.assign(das_char);
      run_experimenters.assign(experimenter_char);
      
      //if (run_method == "TI-bNMR" and run_mode == "1f")
      if (run_method == "TI-bNMR")
      {
         // Read the logged individual variables
         MUD_getIndVars(file_handle, &file_type, &number_of_logged_variables);
         for (unsigned int i = 1; i <= number_of_logged_variables; ++i)
         {
            unsigned int logged_variable_number_of_points;
            unsigned int logged_variable_data_type;
            unsigned int logged_variable_element_size;
            char name[strdim];
            //char description[strdim];
            //char units[strdim];
            //double mean;
            //double stdev;
            //double skew;
            //double min;
            //double max;
            
            MUD_getIndVarName(file_handle, i, name, strdim);
            // history data
            MUD_getIndVarNumData(file_handle, i, &logged_variable_number_of_points);
            MUD_getIndVarElemSize(file_handle, i, &logged_variable_element_size);
            MUD_getIndVarDataType(file_handle, i, &logged_variable_data_type);
            /*
            std::cout << "   file_handle                       = " << file_handle << "\n";
            std::cout << "   file_type                         = " << file_type << "\n";
            std::cout << "   name                              = " << name << "\n";
            std::cout << "   logged_variable_number_of_points  = " << logged_variable_number_of_points << "\n";
            std::cout << "   logged_variable_element_size      = " << logged_variable_element_size << "\n";
            std::cout << "   logged_variable_data_type         = " << logged_variable_data_type << "\n" << std::endl;
            */
            if (strncmp(name,"ILE2:LAS:RDPOWER",14) == 0)
          	{
               laser_power.resize(logged_variable_number_of_points);
               MUD_getIndVarData(file_handle, i, &laser_power.at(0));
          	}
          	if (strncmp(name,"ITE:BIAS:RDVOL",10) == 0 or strncmp(name,"ITW:BIAS:RDVOL",10) == 0)
          	{
          	   isac_target_bias.resize(logged_variable_number_of_points);
               MUD_getIndVarData(file_handle, i, &isac_target_bias.at(0));
               for (auto &itb: isac_target_bias) itb *= 0.001; // convert to kV
          	}
          	if (strncmp(name,"ILE2:BIAS15:RDVOL",13) == 0)
          	{
          	   alkali_cell_bias.resize(logged_variable_number_of_points);
               MUD_getIndVarData(file_handle, i, &alkali_cell_bias.at(0));
               for (auto &acb: alkali_cell_bias) acb *= 0.001; // convert to kV
          	}
          	if (strncmp(name,"ILE2:BIASTUBE:VOL",17) == 0)
          	{
          	   tube_bias.resize(logged_variable_number_of_points);
               MUD_getIndVarData(file_handle, i, &tube_bias.at(0));
          	}
          	if (strncmp(name,"BNMR:HVBIAS:POS:RDVOL",13) == 0 or strncmp(name,"BNQR:HVBIAS:RDVOL",13) == 0) 
          	{
          	   platform_bias.resize(logged_variable_number_of_points);
               MUD_getIndVarData(file_handle, i, &platform_bias.at(0));
               if (run_number >= 45000 and run_number <= 49999)
               {
                  for (auto &pb: platform_bias) pb *= 0.001; // convert to kV
               }
          	}
          	if (strcmp(name,"/Magnet/mag_field") == 0 or strcmp(name,"/magnet/mag_field") == 0)   
          	{
          	   magnetic_field.resize(logged_variable_number_of_points);
               MUD_getIndVarData(file_handle, i, &magnetic_field.at(0));
          	}
          	if (strncmp(name,"ILE2A1:HH:RDCURRENT",13) == 0 or strncmp(name,"ILE2A1:HH:CURRENT",13) == 0)
          	{
          		helmholtz_coil_current.resize(logged_variable_number_of_points);
          		MUD_getIndVarData(file_handle, i, &helmholtz_coil_current.at(0));
          		// Use Zaher's calibration to compute the field (T)
          		// H [G] = (0.175 +/- 0.046 [G]) + (2.2131 +/- 0.0019 [G/A])*( current [A])
	            auto zahers_calibration = [](double current) {return 1e-4*(0.175 + 2.2131*current);};
	            for (auto &I: helmholtz_coil_current) magnetic_field.push_back(zahers_calibration(I));
          	}
          	if (strcmp(name,"/Sample/read_A") == 0 or strcmp(name,"/Sample/Read_A") == 0 or strcmp(name,"/Sample1/read_A") == 0 or strcmp(name,"/Sample1/Read_A") == 0 or strcmp(name,"/Sample/sample_read") == 0 )
          	{
          	   temperature_a.resize(logged_variable_number_of_points);
               MUD_getIndVarData(file_handle, i, &temperature_a.at(0));
          	}
          	if (strcmp(name,"/Sample/read_B") == 0 or strcmp(name,"/Sample/Read_B") == 0 or strcmp(name,"/Sample1/read_B") == 0 or strcmp(name,"/Sample1/Read_B") == 0)
          	{
          	   temperature_b.resize(logged_variable_number_of_points);
               MUD_getIndVarData(file_handle, i, &temperature_b.at(0));
          	} 
         } // loop over number_of_logged_variables
         
         // Read the histograms
         MUD_getHists(file_handle, &file_type, &number_of_histograms);
         for (unsigned int i = 0; i <= number_of_histograms; ++i)
         {
            char histogram_name[strdim];
            unsigned int histogram_bins = 0;
            MUD_getHistTitle(file_handle, i, histogram_name, strdim);
            MUD_getHistNumBins(file_handle, i, &histogram_bins);
            // NMR VECTORS
            if (strcmp(histogram_name,"B+") == 0 or strcmp(histogram_name,"b+") == 0 or strcmp(histogram_name,"Back+") == 0 or strcmp(histogram_name,"back+") == 0)
            {
               raw_histogram_B_p.resize(histogram_bins);
               MUD_getHistData(file_handle, i, &raw_histogram_B_p.at(0) );
            }
             if (strcmp(histogram_name,"B-") == 0 or strcmp(histogram_name,"b-") == 0 or strcmp(histogram_name,"Back-") == 0 or strcmp(histogram_name,"back-") == 0)
            {
               raw_histogram_B_m.resize(histogram_bins);
               MUD_getHistData(file_handle, i, &raw_histogram_B_m.at(0) );
            }
            if (strcmp(histogram_name,"F+") == 0 or strcmp(histogram_name,"f+") == 0 or strcmp(histogram_name,"Front+") == 0 or strcmp(histogram_name,"front+") == 0)
            {
               raw_histogram_F_p.resize(histogram_bins);
               MUD_getHistData(file_handle, i, &raw_histogram_F_p.at(0) );
            }
            if (strcmp(histogram_name,"F-") == 0 or strcmp(histogram_name,"f-") == 0 or strcmp(histogram_name,"Front-") == 0 or strcmp(histogram_name,"front-") == 0)
            {
               raw_histogram_F_m.resize(histogram_bins);
               MUD_getHistData(file_handle, i, &raw_histogram_F_m.at(0) );
            }
            // FREQUENCY VECTOR (for e.g., 1f type runs)
            if (strcmp(histogram_name,"Frequency") == 0 or strcmp(histogram_name,"frequency") == 0 or strcmp(histogram_name,"FREQ") == 0 or strcmp(histogram_name,"freq") == 0)
            {
               raw_histogram_frequency.resize(histogram_bins);
               MUD_getHistData(file_handle, i, &raw_histogram_frequency.at(0) );
            }
            // NQR VECTORS
            if (strcmp(histogram_name,"L+") == 0 or strcmp(histogram_name,"l+") == 0 or strcmp(histogram_name,"Left+") == 0 or strcmp(histogram_name,"left+") == 0)
            {
               raw_histogram_L_p.resize(histogram_bins);
               MUD_getHistData(file_handle, i, &raw_histogram_L_p.at(0) );
            }
             if (strcmp(histogram_name,"L-") == 0 or strcmp(histogram_name,"l-") == 0 or strcmp(histogram_name,"Left-") == 0 or strcmp(histogram_name,"left-") == 0)
            {
               raw_histogram_L_m.resize(histogram_bins);
               MUD_getHistData(file_handle, i, &raw_histogram_L_m.at(0) );
            }
            if (strcmp(histogram_name,"R+") == 0 or strcmp(histogram_name,"r+") == 0 or strcmp(histogram_name,"Right+") == 0 or strcmp(histogram_name,"right+") == 0)
            {
               raw_histogram_R_p.resize(histogram_bins);
               MUD_getHistData(file_handle, i, &raw_histogram_R_p.at(0) );
            }
            if (strcmp(histogram_name,"R-") == 0 or strcmp(histogram_name,"r-") == 0 or strcmp(histogram_name,"Right-") == 0 or strcmp(histogram_name,"right-") == 0)
            {
               raw_histogram_R_m.resize(histogram_bins);
               MUD_getHistData(file_handle, i, &raw_histogram_R_m.at(0) );
            }
         } // loop over number_of_histograms
         
         // calculate raw asymmetry
         //std::cout << "frequency.size() " << raw_histogram_frequency.size() << std::endl;
         //std::cout << "frequency.size() " << raw_histogram_B_p.size() << std::endl;
         //std::cout << "frequency.size() " << raw_histogram_F_p.size() << std::endl;
         //std::cout << "frequency.size() " << raw_histogram_B_m.size() << std::endl;
         //std::cout << "frequency.size() " << raw_histogram_F_m.size() << std::endl;
         
         // calculate the raw asymmetry (lcrplot style)
         for (std::size_t i = 0; i < raw_histogram_frequency.size(); ++i)
         {
            raw_histogram_bin.push_back(i);
            if (run_number >= 40000 and run_number <= 44999) // bnmr
            {
               raw_histogram_asymmetry.push_back(bnmr::asymmetry::calculate(raw_histogram_B_p.at(i),raw_histogram_F_p.at(i),raw_histogram_B_m.at(i),raw_histogram_F_m.at(i)) );
               raw_histogram_dasymmetry.push_back(bnmr::asymmetry::calculate_uncertainty(raw_histogram_B_p.at(i),raw_histogram_F_p.at(i),raw_histogram_B_m.at(i),raw_histogram_F_m.at(i)) );
            }
            if (run_number >= 45000 and run_number <= 49999) // bnqr
            {
               raw_histogram_asymmetry.push_back(bnmr::asymmetry::calculate(raw_histogram_L_p.at(i),raw_histogram_R_p.at(i),raw_histogram_L_m.at(i),raw_histogram_R_m.at(i)) );
               raw_histogram_dasymmetry.push_back(bnmr::asymmetry::calculate_uncertainty(raw_histogram_L_p.at(i),raw_histogram_R_p.at(i),raw_histogram_L_m.at(i),raw_histogram_R_m.at(i)) );
            }
         }
         
         sort_frequencies();
         sort_histograms();
         calculate_asymmetry_matrix_helicities();
         calculate_average_asymmetry();
         
      } // check run_method == TI-bNMR and run_mode == 1f
   } // file_handle == 0
   MUD_closeRead(file_handle);
}


double triumf::bnmr_1f::temperature()
{
   std::vector<float> temperature_ab;
   temperature_ab.insert(temperature_ab.end(), temperature_a.begin(), temperature_a.end());
   temperature_ab.insert(temperature_ab.end(), temperature_b.begin(), temperature_b.end());
   temperature_ab.erase(std::remove(temperature_ab.begin(), temperature_ab.end(), 0), temperature_ab.end());
   return bnmr::statistics::average(temperature_ab);
}


double triumf::bnmr_1f::temperature_error()
{
   std::vector<float> temperature_ab;
   temperature_ab.insert(temperature_ab.end(), temperature_a.begin(), temperature_a.end());
   temperature_ab.insert(temperature_ab.end(), temperature_b.begin(), temperature_b.end());
   temperature_ab.erase(std::remove(temperature_ab.begin(), temperature_ab.end(), 0), temperature_ab.end());
   return bnmr::statistics::standard_deviation(temperature_ab);
}

double triumf::bnmr_1f::field()
{
   return bnmr::statistics::average(magnetic_field);
}


double triumf::bnmr_1f::field_error()
{
   return bnmr::statistics::standard_deviation(magnetic_field);
}


double triumf::bnmr_1f::energy()
{
   std::vector<double> energies;
   for (std::size_t i = 0; i < isac_target_bias.size(); ++i)
   {
      energies.push_back(isac_target_bias.at(i) - alkali_cell_bias.at(i) - platform_bias.at(i));
   }
   return bnmr::statistics::average(energies);
}


double triumf::bnmr_1f::energy_error()
{
   std::vector<double> energies;
   for (std::size_t i = 0; i < isac_target_bias.size(); ++i)
   {
      energies.push_back(isac_target_bias.at(i) - alkali_cell_bias.at(i) - platform_bias.at(i));
   }
   return bnmr::statistics::standard_deviation(energies);
}


void triumf::bnmr_1f::print_info()
{
   std::cout << "\n";
   std::cout << "    " << colour::red << file_name << colour::reset << "\n";
   std::cout << "    " << run_title << "\n";
   std::cout << "    " << colour::cyan << std::asctime(std::localtime(&run_start_time)) << colour::reset;
   std::cout << "    " << colour::green << run_year << " " << run_number << colour::reset;
   std::cout << "    " << run_lab << " " << run_area << colour::reset  << " - " << run_method << " (" << run_mode << ") - " << run_data_acquisition_system << " - " <<  std::setprecision(2) << std::fixed << colour::magenta << run_elapsed_time/60.0 << colour::reset << " min\n";
   std::cout << "    Exp. M" << run_experiment_number << "    [" << run_experimenters << "]\n";
   std::cout << "    " << run_sample << " " << run_sample_orientation << "\n";
   std::cout << std::setprecision(4) << std::fixed;
   std::cout << "    Beam Energy    = " << colour::yellow << std::setw(8) << std::right << energy() << colour::reset;
   std::cout << " ± " << colour::yellow << std::setw(7) << std::right << energy_error() << colour::reset << " keV\n";
   std::cout << "    Magnetic Field = " << colour::blue << std::setw(8) << std::right << field() << colour::reset << " ± " << colour::blue << std::setw(7) << std::right << field_error() << colour::reset << " T\n";
   std::cout << "    Temperature    = " << colour::green << std::setw(8) << std::right << temperature() << colour::reset << " ± " << colour::green << std::setw(7) << std::right << temperature_error() << colour::reset << " K\n";
   std::cout << "\n";
}

void triumf::bnmr_1f::print_raw_asymmetry()
{
   for (std::size_t i = 0; i < raw_histogram_bin.size(); ++i)
   {
      std::cout << raw_histogram_bin.at(i) << "\t";
      std::cout << raw_histogram_frequency.at(i) << "\t";
      std::cout << raw_histogram_asymmetry.at(i) << "\t";
      std::cout << raw_histogram_dasymmetry.at(i) << "\n";
   }
}

void triumf::bnmr_1f::sort_frequencies()
{
   // Make a temporary vector, sort it, then erase common adjacent elements
   std::vector<unsigned int> tmp_frequency(raw_histogram_frequency);
   std::sort(tmp_frequency.begin(), tmp_frequency.end());
   tmp_frequency.erase(std::unique(tmp_frequency.begin(), tmp_frequency.end()), tmp_frequency.end());
   sorted_histogram_frequency = tmp_frequency;
}

void triumf::bnmr_1f::sort_histograms()
{
   // calculate the frequency-counts "matrix" for each histogram_bins
   // loop over frequencies
   for (std::size_t i = 0; i < sorted_histogram_frequency.size(); ++i)
   {
      // create some temporary vectors...
      std::vector<unsigned int> B_p_tmp;
      std::vector<unsigned int> B_m_tmp;
      std::vector<unsigned int> F_p_tmp;
      std::vector<unsigned int> F_m_tmp;
      std::vector<unsigned int> L_p_tmp;
      std::vector<unsigned int> L_m_tmp;
      std::vector<unsigned int> R_p_tmp;
      std::vector<unsigned int> R_m_tmp;
      std::vector<unsigned int> NBMB_p_tmp;
      //std::vector<unsigned int> NBMB_m_tmp;
      //std::vector<unsigned int> NBMF_p_tmp;
      //std::vector<unsigned int> NBMF_m_tmp;
      // ...then loop over ALL points
      for (std::size_t j = 0; j < raw_histogram_frequency.size(); ++j)
      {
         if ( sorted_histogram_frequency.at(i) == raw_histogram_frequency.at(j) )
         {
            //
            // This check for non-zero counts in BOTH detector pairs fixes cases
            // out-of-range std::vector<double> issues, resultant from F/B +/-
            // pairs have DIFFERENT numbers of entries!
            //
            if (run_number >= 40000 and run_number <= 44999)
            {
               /*
               if ( raw_histogram_B_p.at(j) != 0 ) B_p_tmp.push_back( raw_histogram_B_p.at(j) );
               if ( raw_histogram_B_m.at(j) != 0 ) B_m_tmp.push_back( raw_histogram_B_m.at(j) );
               if ( raw_histogram_F_p.at(j) != 0 ) F_p_tmp.push_back( raw_histogram_F_p.at(j) );
               if ( raw_histogram_F_m.at(j) != 0 ) F_m_tmp.push_back( raw_histogram_F_m.at(j) );
               */
               if ( raw_histogram_B_p.at(j) != 0 and raw_histogram_F_p.at(j) != 0  ) { B_p_tmp.push_back( raw_histogram_B_p.at(j) ); F_p_tmp.push_back( raw_histogram_F_p.at(j) ); }
               if ( raw_histogram_B_m.at(j) != 0 and raw_histogram_F_m.at(j) != 0  ) { B_m_tmp.push_back( raw_histogram_B_m.at(j) ); F_m_tmp.push_back( raw_histogram_F_m.at(j) ); }
            }
            if (run_number >= 45000 and run_number <= 49999)
            {
               /*
               if ( raw_histogram_L_p.at(j) != 0 ) L_p_tmp.push_back( raw_histogram_L_p.at(j) );
               if ( raw_histogram_L_m.at(j) != 0 ) L_m_tmp.push_back( raw_histogram_L_m.at(j) );
               if ( raw_histogram_R_p.at(j) != 0 ) R_p_tmp.push_back( raw_histogram_R_p.at(j) );
               if ( raw_histogram_R_m.at(j) != 0 ) R_m_tmp.push_back( raw_histogram_R_m.at(j) );
               */
               if ( raw_histogram_L_p.at(j) != 0 and raw_histogram_R_p.at(j) != 0  ) { L_p_tmp.push_back( raw_histogram_L_p.at(j) ); R_p_tmp.push_back( raw_histogram_R_p.at(j) ); }
               if ( raw_histogram_L_m.at(j) != 0 and raw_histogram_R_m.at(j) != 0  ) { L_m_tmp.push_back( raw_histogram_L_m.at(j) ); R_m_tmp.push_back( raw_histogram_R_m.at(j) ); }
            }
            /*
            if ( vNBMB_p.at(j) != 0 ) NBMB_p_tmp.push_back( vNBMB_p.at(j) );
            if ( vNBMB_m.at(j) != 0 ) NBMB_m_tmp.push_back( vNBMB_m.at(j) );
            if ( vNBMF_p.at(j) != 0 ) NBMF_p_tmp.push_back( vNBMF_p.at(j) );
            if ( vNBMF_m.at(j) != 0 ) NBMF_m_tmp.push_back( vNBMF_m.at(j) );
            */
            //if ( vNBMB_p.at(j) != 0 and vNBMF_p.at(j) != 0  ) { NBMB_p_tmp.push_back( vNBMB_p.at(j) ); NBMF_p_tmp.push_back( vNBMF_p.at(j) ); }
            //if ( vNBMB_m.at(j) != 0 and vNBMF_m.at(j) != 0  ) { NBMB_m_tmp.push_back( vNBMB_m.at(j) ); NBMF_m_tmp.push_back( vNBMF_m.at(j) ); }
         }
      }
      // push the tmp vectors into the histogram matrix
      B_p.push_back( B_p_tmp ); B_p_tmp.clear();
      B_m.push_back( B_m_tmp ); B_m_tmp.clear();
      F_p.push_back( F_p_tmp ); F_p_tmp.clear();
      F_m.push_back( F_m_tmp ); F_m_tmp.clear();
      L_p.push_back( L_p_tmp ); L_p_tmp.clear();
      L_m.push_back( L_m_tmp ); L_m_tmp.clear();
      R_p.push_back( R_p_tmp ); R_p_tmp.clear();
      R_m.push_back( R_m_tmp ); R_m_tmp.clear();
      //NBMB_p.push_back( NBMB_p_tmp ); NBMB_p_tmp.clear();
      //NBMB_m.push_back( NBMB_m_tmp ); NBMB_m_tmp.clear();
      //NBMF_p.push_back( NBMF_p_tmp ); NBMF_p_tmp.clear();
      //NBMF_m.push_back( NBMF_m_tmp ); NBMF_m_tmp.clear();
   } 
}

void triumf::bnmr_1f::calculate_asymmetry_matrix_helicities()
{
   for (std::size_t i = 0; i < sorted_histogram_frequency.size(); ++i)
   {
      std::vector<double> A_p_tmp;
      std::vector<double> dA_p_tmp;
      std::vector<double> A_m_tmp;
      std::vector<double> dA_m_tmp;
      //std::vector<double> NBMA_p_tmp;
      //std::vector<double> dNBMA_p_tmp;
      //std::vector<double> NBMA_m_tmp;
      //std::vector<double> dNBMA_m_tmp;
      
      // Positive Helicity
      
      if (run_number >= 40000 and run_number <= 44999)  // bnmr
      {
         for (std::size_t j = 0; j < B_p.at(i).size(); ++j )
         {
            A_p_tmp.push_back(bnmr::asymmetry::calculate(B_p.at(i).at(j),F_p.at(i).at(j)) );
            dA_p_tmp.push_back(bnmr::asymmetry::calculate_uncertainty(B_p.at(i).at(j),F_p.at(i).at(j)) );
            //NBMA_p_tmp.push_back( ( static_cast<double>(NBMB_p.at(i).at(j))-NBMF_p.at(i).at(j))/(NBMB_p.at(i).at(j)+NBMF_p.at(i).at(j)) );
            //dNBMA_p_tmp.push_back( 2*std::sqrt( static_cast<double>(NBMB_p.at(i).at(j))*NBMF_p.at(i).at(j) )/std::pow( NBMB_p.at(i).at(j) + NBMF_p.at(i).at(j) ,1.5) );
         }
      }
      if (run_number >= 45000 and run_number <= 49999) // bnqr
      {
         for (std::size_t j = 0; j < L_p.at(i).size(); ++j )
         {
            A_p_tmp.push_back(bnmr::asymmetry::calculate(L_p.at(i).at(j),R_p.at(i).at(j)) );
            dA_p_tmp.push_back(bnmr::asymmetry::calculate_uncertainty(L_p.at(i).at(j),R_p.at(i).at(j)) );
            //NBMA_p_tmp.push_back( ( static_cast<double>(NBMB_p.at(i).at(j))-NBMF_p.at(i).at(j))/(NBMB_p.at(i).at(j)+NBMF_p.at(i).at(j)) );
            //dNBMA_p_tmp.push_back( 2*std::sqrt( static_cast<double>(NBMB_p.at(i).at(j))*NBMF_p.at(i).at(j) )/std::pow( NBMB_p.at(i).at(j) + NBMF_p.at(i).at(j) ,1.5) );
         }
      }
      
      A_p.push_back(A_p_tmp); A_p_tmp.clear();
      dA_p.push_back(dA_p_tmp); dA_p_tmp.clear();
      //NBMA_p.push_back( NBMA_p_tmp );
      //NBMA_p_tmp.clear();
      //dNBMA_p.push_back( dNBMA_p_tmp );
      //dNBMA_p_tmp.clear();
      
      // Negative Helicity
      
      if (run_number >= 40000 and run_number <= 44999)  // bnmr
      {
         for (std::size_t j = 0; j < B_m.at(i).size(); ++j )
         {
            A_m_tmp.push_back(bnmr::asymmetry::calculate(B_m.at(i).at(j),F_m.at(i).at(j)) );
            dA_m_tmp.push_back(bnmr::asymmetry::calculate_uncertainty(B_m.at(i).at(j),F_m.at(i).at(j)) );
            //NBMA_m_tmp.push_back( ( static_cast<double>(NBMB_m.at(i).at(j))-NBMF_m.at(i).at(j))/(NBMB_m.at(i).at(j)+NBMF_m.at(i).at(j)) );
            //dNBMA_m_tmp.push_back( 2*std::sqrt( static_cast<double>(NBMB_m.at(i).at(j))*NBMF_m.at(i).at(j) )/std::pow( NBMB_m.at(i).at(j) + NBMF_m.at(i).at(j) ,1.5) );
         }
      }
      if (run_number >= 45000 and run_number <= 49999) // bnqr
      {
         for (std::size_t j = 0; j < L_m.at(i).size(); ++j )
         {
            A_m_tmp.push_back(bnmr::asymmetry::calculate(L_m.at(i).at(j),R_m.at(i).at(j)) );
            dA_m_tmp.push_back(bnmr::asymmetry::calculate_uncertainty(L_m.at(i).at(j),R_m.at(i).at(j)) );
            //NBMA_m_tmp.push_back( ( static_cast<double>(NBMB_m.at(i).at(j))-NBMF_m.at(i).at(j))/(NBMB_m.at(i).at(j)+NBMF_m.at(i).at(j)) );
            //dNBMA_m_tmp.push_back( 2*std::sqrt( static_cast<double>(NBMB_m.at(i).at(j))*NBMF_m.at(i).at(j) )/std::pow( NBMB_m.at(i).at(j) + NBMF_m.at(i).at(j) ,1.5) );
         }
      }
            
      A_m.push_back(A_m_tmp); A_m_tmp.clear();
      dA_m.push_back(dA_m_tmp); dA_m_tmp.clear();
      //NBMA_m.push_back( NBMA_m_tmp );
      //NBMA_m_tmp.clear();
      //dNBMA_m.push_back( dNBMA_m_tmp );
      //dNBMA_m_tmp.clear();
   }
}

void triumf::bnmr_1f::calculate_average_asymmetry()
{
   // compute A averages
   for (std::size_t i = 0; i < sorted_histogram_frequency.size(); ++i )
   {
      asymmetry_p.push_back(bnmr::statistics::weighted_average(A_p.at(i), dA_p.at(i)));
      dasymmetry_p.push_back(bnmr::statistics::weighted_standard_deviation(dA_p.at(i)));
      asymmetry_m.push_back(bnmr::statistics::weighted_average(A_m.at(i), dA_m.at(i)));
      dasymmetry_m.push_back(bnmr::statistics::weighted_standard_deviation(dA_m.at(i)));
      asymmetry.push_back(0.5*(asymmetry_p.at(i) - asymmetry_m.at(i)));
      dasymmetry.push_back(0.5*std::sqrt(dasymmetry_p.at(i)*dasymmetry_p.at(i) + dasymmetry_m.at(i)*dasymmetry_m.at(i)));
   }
}

void triumf::bnmr_1f::save_asymmetry()
{
   std::string outputfilename( std::to_string(run_year) + "-" + std::to_string(run_number) + ".dat" );
   std::ofstream outputfile;
   outputfile.open( outputfilename.c_str() );
   //if (argc == 5)
   //{
   //   outputfile << "# Combined Runs: " << year << "-" << run << " & " << year2 << "-" << run2 << "\n";
   //   outputfile << "#Frequency(Hz)\tA+(a.u.)\tdA+(a.u.\tA-(a.u.)\tdA-(a.u.)\tA(a.u.)\tdA(a.u.)\n";
   //}
   //else
   //{
   //   outputfile << "#Frequency(Hz)\tA+(a.u.)\tdA+(a.u.\tA-(a.u.)\tdA-(a.u.)\tA(a.u.)\tdA(a.u.)\n";
   //}
   //
   outputfile << "# Year: " << run_year << "\tRun:" << run_number << "\n";
   outputfile << "#\n";
   outputfile << std::setprecision(4) << std::fixed;
   outputfile << "# Energy = " << energy() << " +/- " << energy_error() << " keV\n";
   outputfile << "# Field = " << field() << " +/- " << field_error() << " T\n";
   outputfile << "# Temperature = " << temperature() << " +/- " << temperature_error() << " K\n";
   outputfile << "#\n";
   outputfile << "#Frequency(Hz)\tA+(a.u.)\tdA+(a.u.)\tA-(a.u.)\tdA-(a.u.)\tA(a.u.)\tdA(a.u.)\n";
   for (std::size_t i = 0; i < sorted_histogram_frequency.size(); ++i)
   {
       outputfile << std::setprecision(0) << std::fixed;
       outputfile << sorted_histogram_frequency.at(i) << "\t";
       outputfile << std::setprecision(9) << std::fixed;
       outputfile << asymmetry_p.at(i) << "\t" << dasymmetry_p.at(i) << "\t";
       outputfile << asymmetry_m.at(i) << "\t" << dasymmetry_m.at(i) << "\t";
       outputfile << asymmetry.at(i) << "\t" << dasymmetry.at(i) << "\n";
   }
   outputfile.close();
}

void triumf::bnmr_1f::remove_bad_bins()
{
   std::cout << "\n";
   std::cout << "    Remove bad bins for run " << run_number << " in " << run_year << "\n";
   std::cout << "    Enter comma seperated bin numbers/ranges to be removed [or leave blank].\n";
   std::cout << "    (e.g.,: 100,430-455,560)\n\n";
   std::cout << "    Press ENTER to proceed\n" << std::endl;
   
   std::string bad_bins;
   // Get the bad_bins string from std::cin...
   std::getline(std::cin, bad_bins);
   // ...convert it to a vector of ints...
   std::vector<int> the_bad_bins = pr::ParseData(bad_bins);
   // ...sort it by descending bin number...
   std::sort(the_bad_bins.begin(), the_bad_bins.end(), std::greater<int>());
   // ...and remove duplicate entries.
   the_bad_bins.erase(std::unique(the_bad_bins.begin(), the_bad_bins.end()), the_bad_bins.end());
   
   // Print the_bad_bins
   /*
   std::cout << "\n";
   for (auto &tbb : the_bad_bins) std::cout << tbb << " ";
   std::cout << std::endl;
   */
   
   // erase the bad bins
   for (auto &tbb: the_bad_bins)
   {
      if (raw_histogram_bin.size() != 0) raw_histogram_bin.erase(raw_histogram_bin.begin() + tbb);
      if (raw_histogram_frequency.size() != 0) raw_histogram_frequency.erase(raw_histogram_frequency.begin() + tbb);
      if (raw_histogram_B_p.size() != 0) raw_histogram_B_p.erase(raw_histogram_B_p.begin() + tbb);
      if (raw_histogram_B_m.size() != 0) raw_histogram_B_m.erase(raw_histogram_B_m.begin() + tbb);
      if (raw_histogram_F_p.size() != 0) raw_histogram_F_p.erase(raw_histogram_F_p.begin() + tbb);
      if (raw_histogram_F_m.size() != 0) raw_histogram_F_m.erase(raw_histogram_F_m.begin() + tbb);
      if (raw_histogram_L_p.size() != 0) raw_histogram_L_p.erase(raw_histogram_L_p.begin() + tbb);
      if (raw_histogram_L_m.size() != 0) raw_histogram_L_m.erase(raw_histogram_L_m.begin() + tbb);
      if (raw_histogram_R_p.size() != 0) raw_histogram_R_p.erase(raw_histogram_R_p.begin() + tbb);
      if (raw_histogram_R_m.size() != 0) raw_histogram_R_m.erase(raw_histogram_R_m.begin() + tbb);
      if (raw_histogram_asymmetry.size() != 0) raw_histogram_asymmetry.erase(raw_histogram_asymmetry.begin() + tbb);
      if (raw_histogram_dasymmetry.size() != 0) raw_histogram_dasymmetry.erase(raw_histogram_dasymmetry.begin() + tbb);
      /*
      // this code erases by ENTRY, not INDEX!
      if (raw_histogram_bin.size() != 0)        raw_histogram_bin.erase(std::remove(raw_histogram_bin.begin(), raw_histogram_bin.end(), tbb), raw_histogram_bin.end());
      if (raw_histogram_frequency.size() != 0)  raw_histogram_frequency.erase(std::remove(raw_histogram_frequency.begin(), raw_histogram_frequency.end(), tbb), raw_histogram_frequency.end());
      if (raw_histogram_B_p.size() != 0)        raw_histogram_B_p.erase(std::remove(raw_histogram_B_p.begin(), raw_histogram_B_p.end(), tbb), raw_histogram_B_p.end());
      if (raw_histogram_B_m.size() != 0)        raw_histogram_B_m.erase(std::remove(raw_histogram_B_m.begin(), raw_histogram_B_m.end(), tbb), raw_histogram_B_m.end());
      if (raw_histogram_F_p.size() != 0)        raw_histogram_F_p.erase(std::remove(raw_histogram_F_p.begin(), raw_histogram_F_p.end(), tbb), raw_histogram_F_p.end());
      if (raw_histogram_F_m.size() != 0)        raw_histogram_F_m.erase(std::remove(raw_histogram_F_m.begin(), raw_histogram_F_m.end(), tbb), raw_histogram_F_m.end());
      if (raw_histogram_asymmetry.size() != 0)  raw_histogram_asymmetry.erase(std::remove(raw_histogram_asymmetry.begin(), raw_histogram_asymmetry.end(), tbb), raw_histogram_asymmetry.end());
      if (raw_histogram_dasymmetry.size() != 0) raw_histogram_dasymmetry.erase(std::remove(raw_histogram_dasymmetry.begin(), raw_histogram_dasymmetry.end(), tbb), raw_histogram_dasymmetry.end());
      */
   }
   
   // recalculate all of the derived histograms
   sorted_histogram_frequency.clear();
   B_p.clear();
   B_m.clear();
   F_p.clear();
   F_m.clear();
   A_p.clear();
   dA_p.clear();
   A_m.clear();
   dA_m.clear();  
   asymmetry_p.clear();
   dasymmetry_p.clear();
   asymmetry_m.clear();
   dasymmetry_m.clear();
   asymmetry.clear();
   dasymmetry.clear();
   
   sort_frequencies();
   sort_histograms();
   calculate_asymmetry_matrix_helicities();
   calculate_average_asymmetry();
}

