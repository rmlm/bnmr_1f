#ifndef BNMR_H
#define BNMR_H

#include <string>
#include <vector>

namespace triumf
{
   class bnmr
   {
   public :
      
      int file_handle;
      unsigned int p_type;
      unsigned int p_number;
      std::string mud_file;
      
      unsigned int Year;
      unsigned int RunNumber;
      unsigned int ExperimentNumber;
      unsigned int TimeElapsed;
      unsigned int TimeBegin;
      unsigned int TimeEnd;
      
      std::string Experimenter;
      std::string Method;
      std::string Mode; // Insert
      std::string Title;
      std::string Sample;
      std::string Orientation;
      std::string Area;
      std::string Apparatus;
      std::string DAS;
      std::string Lab;
      
      double Temperature_A;
      double TemperatureError_A;
      double TemperatureMin_A;
      double TemperatureMax_A;
      
      double Temperature_B;
      double TemperatureError_B;
      double TemperatureMin_B;
      double TemperatureMax_B;
      
      double Temperature;
      double TemperatureError;
      double TemperatureMin;
      double TemperatureMax;
      
      double MagneticField;
      double MagneticFieldError;
      double MagneticFieldMin;
      double MagneticFieldMax;
      
      double ImplantationEnergy;
      double ImplantationEnergyError;
      double ImplantationEnergyMin;
      double ImplantationEnergyMax;
      
      double AlkaliCellBias;
      double AlkaliCellBiasError;
      double AlkaliCellBiasMin;
      double AlkaliCellBiasMax;
      
      double ISACTargetBias;
      double ISACTargetBiasError;
      double ISACTargetBiasMin;
      double ISACTargetBiasMax;
   
      double PlatformBias;
      double PlatformBiasError;
      double PlatformBiasMin;
      double PlatformBiasMax;
   
      double LaserPower;
      double LaserPowerError;
      double LaserPowerMin;
      double LaserPowerMax;
   }; // class bnmr
} // namespace triumf
#endif // BNMR_H


   /*
   void PrintDescription();
   void PrintVariables();
   //void Print(std::vector<unsigned int> &V,);
   
   unsigned int BinningFactor = 1;
   
   //void SetIsotope(char *isotope);
   //void GetIsotope();
   
protected :
   void wgetRun();
   void GetRun();
   void ReadRunDescription();
   void ReadLoggedVariables();
   void ReadHistograms();
   
   //std::string Isotope;
   //double Lifetime = 1.2906;
   //double LifetimeError = 0.0005;
   
   
   std::vector<unsigned int> vFrequency;
   std::vector<unsigned int> vVoltage;
   std::vector<unsigned int> vField;
   
   std::vector<unsigned int> vB_p;
   std::vector<unsigned int> vB_m;
   std::vector<unsigned int> vF_p;
   std::vector<unsigned int> vF_m;
   
   std::vector<unsigned int> vL_p;
   std::vector<unsigned int> vL_m;
   std::vector<unsigned int> vR_p;
   std::vector<unsigned int> vR_m;
   
   std::vector<unsigned int> vNBMB_p;
   std::vector<unsigned int> vNBMB_m;
   std::vector<unsigned int> vNBMF_p;
   std::vector<unsigned int> vNBMF_m;
   */
   
   
   /*
   std::vector<unsigned int> vBorL_p;
   std::vector<unsigned int> vBorL_m;
   std::vector<unsigned int> vForR_p;
   std::vector<unsigned int> vForR_m;
   
   std::vector<double> vA_p;
   std::vector<double> vdA_p;
   std::vector<double> vA_m;
   std::vector<double> vdA_m;
   std::vector<double> vA;
   std::vector<double> vdA;
      
   std::vector<unsigned int> vNBMB_p;
   std::vector<unsigned int> vNBMB_m;
   std::vector<unsigned int> vNBMF_p;
   std::vector<unsigned int> vNBMF_m;
   
   std::vector<double> vNBMA_p;
   std::vector<double> vdNBMA_p;
   std::vector<double> vNBMA_m;
   std::vector<double> vdNBMA_m;
   
   std::vector<unsigned int> vVoltage;
   std::vector<unsigned int> vFrequency;
   */



