#ifndef BNMR_STATISTICS_H
#define BNMR_STATISTICS_H

#include <algorithm>
#include <cmath>
#include <iostream>
#include <numeric>
#include <vector>

namespace bnmr
{
   namespace statistics
   {
      //
      template <typename T>
      double average(const std::vector<T> &values)
      {
         return std::accumulate(values.begin(), values.end(), 0.0)/values.size();
      }
      
      //
      template <typename T>
      double standard_deviation(const std::vector<T> &values, bool sample = true)
      {
         auto mean = std::accumulate(values.begin(), values.end(), 0.0)/values.size();
         auto sum_of_deviations_squared = 0.0;
         std::for_each(std::begin(values), std::end(values), [&](const double value)
         {
            sum_of_deviations_squared += (value-mean)*(value-mean);
         }
         );
         if (sample == false) return std::sqrt(sum_of_deviations_squared/values.size());
         return std::sqrt(sum_of_deviations_squared/(values.size()-1.0));
      }
      
      //
      template <typename T>
      double weighted_average(const std::vector<T> &values, const std::vector<T> &errors)
      {
         // return 0 if vectors are not the same size
         if (values.size() != errors.size())
         {
            std::cout << "size of value and error vectors is not the same!" << std::endl;
            return 0.0;
         }
         
         // return 0 if one of the errors is 0
         std::vector<double> weights;//(values.size());
         for (auto e: errors)
         {
            if (e <= 0.0)
            {
               std::cout << "error value = 0!" << std::endl;
               return 0.0;
            }
            weights.push_back(1.0/e/e); 
         }
         
         auto numerator = std::inner_product(values.begin(), values.end(), weights.begin(), 0.0);
         auto denominator = std::accumulate(weights.begin(), weights.end(), 0.0);
         return numerator/denominator;
      }
      
      // 
      template <typename T>
      double weighted_standard_deviation(const std::vector<T> &errors)
      {
         // return 0 if one of the errors is 0
         std::vector<double> weights;//(values.size());
         for (auto e: errors)
         {
            if (e <= 0.0)
            {
               std::cout << "error value = 0!" << std::endl;
               return 0.0;
            }
            weights.push_back(1.0/e/e); 
         }
         auto denominator = std::accumulate(weights.begin(), weights.end(), 0.0);
         return std::sqrt(1.0/denominator);
      }
   } // namespace statistics
} // namespace bnmr
#endif // BNMR_STATISTICS_H
