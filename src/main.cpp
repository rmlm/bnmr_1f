
#include <algorithm>
#include <chrono>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <numeric>
#include <sstream>
#include <string>
#include <thread>
#include <vector>

#include <unistd.h>

#include <TApplication.h>
#include <TCanvas.h>
#include <TColor.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TStyle.h>
#include <TSystem.h>

#include "bnmr_1f.h"
#include "bnmr_1f_combined.h"
#include "colour.h"

int main(int argc, char *argv[])
{
   std::vector<unsigned int> year;
   std::vector<unsigned int> run;
   
   // Need at least a pair of command line arguments arguments (year and a run number)...
   if (argc >= 3 and (argc & 1) == 1)
   {
      // ...so push_back every second argument to the appropriate vectors...
      for (int i = 1; i < argc; ++++i) year.push_back(static_cast<unsigned int>(std::atoi(argv[i])) );
      for (int i = 2; i < argc; ++++i) run.push_back(static_cast<unsigned int>(std::atoi(argv[i])) );
   }
   else
   {
      // ...or otherwise print out some usage info.
      std::cout << "\n";
      std::cout << "    " + colour::green + "bnmr_1f" + colour::reset + " : " + colour::yellow + "a command line tool to process 1f mode β-NMR data" + colour::reset + "\n\n";
      std::cout << "    - requires year/run pairs as inputs; any number can be specified.\n";        
      std::cout << "    - example usage:\n\n";
      std::cout << "      ./bnmr_1f <year> <run>\n";
      std::cout << "      ./bnmr_1f <year_1> <run_1> <year_2> <run_2>\n";
      std::cout << "      ./bnmr_1f <year_1> <run_1> <year_2> <run_2> ... <year_n> <run_n>\n\n";
      return EXIT_FAILURE;
   }
   
   // fail on unequal number of year/run inputs
   if (year.size() != run.size())
   {
      return EXIT_FAILURE;
   }
   
   // fill the vector 1f runs
   std::vector<triumf::bnmr_1f> resonances;
   for (std::size_t i = 0; i < year.size(); ++i)
   {
      resonances.push_back(triumf::bnmr_1f(year.at(i), run.at(i)));
   }
   
   // check if any arn't 1f mode data
   for (auto &r: resonances)
   {
      if (r.run_mode != "1f")
      {
         std::cout << r.run_year << ":" << r.run_number << " is not a 1f run.\n";
         //return EXIT_FAILURE;
      }
   }
   
   
   
   TApplication root_application("root_application", &argc, argv);
   TQObject::Connect("TCanvas", "Closed()", "TApplication", &root_application, "Terminate()");
   
   // Set the aesthetics for the ROOT graphics
   gStyle->SetPalette(57);
   gStyle->SetPadTickX(1);
   gStyle->SetPadTickY(1); 
   gStyle->SetPadGridX(0);
   gStyle->SetPadGridY(0);
   gStyle->SetPadTopMargin(0.15);
   gStyle->SetPadBottomMargin(0.15);
   gStyle->SetPadLeftMargin(0.15);
   gStyle->SetPadRightMargin(0.10);
   gStyle->SetLabelFont(42,"xyz");
   gStyle->SetLabelSize(0.035,"xyz");
   gStyle->SetTitleFont(42,"xyz");
   gStyle->SetTitleSize(0.05,"xyz");
   gStyle->SetTitleOffset(1.2,"xyz");
   gStyle->SetTitleFont(42,"1");
   gStyle->SetTitleSize(0.05,"1");
   gStyle->SetTitleBorderSize(0);
   gStyle->SetHistMinimumZero(kTRUE);
   gStyle->SetStripDecimals(kFALSE);
   gStyle->SetOptStat(0);
   gStyle->SetOptFit(0);
   gStyle->SetOptTitle(1);
   gStyle->SetCanvasColor(0);
   gStyle->SetCanvasBorderSize(0);
   gStyle->SetCanvasBorderMode(0);
   gStyle->SetFrameBorderMode(0);
   gStyle->SetFrameFillColor(0);
   gStyle->SetPadBorderMode(0);
   gStyle->SetPadBorderSize(0);
   gStyle->SetPadColor(0);
   gStyle->SetNumberContours(255);
   
   TCanvas root_canvas("root_canvas", ": bnmr_1f : ", 800, 600);
   root_canvas.SetFrameFillColor(TColor::GetColor(255,255,204));
   root_canvas.Draw();
   //root_canvas.Modified();
   root_canvas.Update();
   
   // Don't start the application thread until *after* the checks on the runs.
   std::thread root_application_thread(&TApplication::Run, &root_application, kTRUE);
   std::this_thread::sleep_for(std::chrono::milliseconds(10));
   // remove the bad bins
   for (auto &r: resonances)
   {
      r.print_info();
      //r.print_raw_asymmetry();
      TGraphErrors bad_bins_graph(r.raw_histogram_asymmetry.size(), r.raw_histogram_bin.data(), r.raw_histogram_asymmetry.data(), 0, r.raw_histogram_dasymmetry.data());
      std::string graph_title = "#splitline{Year: " + std::to_string(r.run_year) + ", Run: " + std::to_string(r.run_number) + "}{" + r.run_title + "};Bin Number;(Raw) Asymmetry";
      bad_bins_graph.SetName("bad_bins_graph");
      bad_bins_graph.SetTitle(graph_title.c_str());
      //bad_bins_graph.SetMarkerStyle(kFullDotMedium);
      //bad_bins_graph.SetMarkerColor(kBlack);
      bad_bins_graph.SetLineColor(kBlack);
      bad_bins_graph.SetFillColor(kBlack);
      //bad_bins_graph.SetLineWidth(1);
      //root_canvas.Divide(1,2);
      //root_canvas.cd();
      //root_canvas.Draw();
      bad_bins_graph.Draw("AP");
      //root_canvas.Modified();
      root_canvas.Update();
      //gSystem->ProcessEvents();
      
      r.remove_bad_bins();
      
      //root_canvas.Clear();
      //root_canvas.Update(); 
   }
   
   
   // create combine the runs into a single object
   triumf::bnmr_1f_combined combined_resonances(resonances);
   combined_resonances.print_info();
   
   TGraphErrors combined_resonances_asymmetry(combined_resonances.asymmetry.size(), combined_resonances.frequency.data(), combined_resonances.asymmetry.data(), 0 , combined_resonances.dasymmetry.data());
   combined_resonances_asymmetry.SetName("combined_resonances_asymmetry");
   combined_resonances_asymmetry.SetTitle(";Frequency (Hz);Asymmetry");
   //combined_resonances_asymmetry.SetMarkerStyle(kFullDotMedium);
   //combined_resonances_asymmetry.SetMarkerColor(kBlack);
   combined_resonances_asymmetry.SetLineColor(kBlack);
   combined_resonances_asymmetry.SetFillColor(kBlack);
   //combined_resonances_asymmetry.SetLineWidth(1);
   combined_resonances_asymmetry.Draw("AP");
   //root_canvas.Modified();
   root_canvas.Update();
   //gSystem->ProcessEvents();
   
   std::cout << "    Would you like to rebin the asymmetry? [yes/(no)]\n";
   std::string rebin_the_asymmetry;
   std::cin >> rebin_the_asymmetry;
   bool has_been_rebinned = false;
   if (rebin_the_asymmetry == "y" or rebin_the_asymmetry == "yes")
   {
      has_been_rebinned = true;
      do
      {
         // find all the "good" binning factors
         std::vector<unsigned int> good_binning_factor;
         for (std::size_t i = 1; i <= combined_resonances.frequency.size(); ++i)
         {
            if (combined_resonances.frequency.size() % i == 0) good_binning_factor.push_back(i);
         }
         
         std::cout << "    There are currently " << combined_resonances.frequency.size() << " frequency bins.\n";
         std::cout << "    - good binning factors are: ";
         for (auto &gbf: good_binning_factor) std::cout << gbf << " ";
         
         std::cout << "\n\n    Enter a binning factor (> 0): \n";
         
         unsigned int rebinning_factor;
         std::cin >> rebinning_factor;
         
         if (std::find(good_binning_factor.begin(), good_binning_factor.end(), rebinning_factor) == good_binning_factor.end())
         {
            std::cout << "\n    WARNING : bad binning factor!\n";
            std::cout << "    - some bins on the high-frequency side will be omitted.\n" << std::endl;
         }
         
         combined_resonances.rebin(rebinning_factor);
         TGraphErrors rebinned(combined_resonances.binned_asymmetry.size(), combined_resonances.binned_frequency.data(), combined_resonances.binned_asymmetry.data(), 0, combined_resonances.binned_dasymmetry.data());
         combined_resonances_asymmetry = rebinned;
         combined_resonances_asymmetry.SetName("combined_resonances_asymmetry");
         combined_resonances_asymmetry.SetTitle(";Frequency (Hz);Asymmetry");
         //combined_resonances_asymmetry.SetMarkerStyle(kFullDotMedium);
         //combined_resonances_asymmetry.SetMarkerColor(kBlack);
         //combined_resonances_asymmetry.SetLineColor(kBlack);
         //combined_resonances_asymmetry.SetLineWidth(1);
         combined_resonances_asymmetry.Draw("AP");
         root_canvas.Modified();
         root_canvas.Update();
         //gSystem->ProcessEvents();
         
         // check if you want some different rebinning
         std::cout << "    Would you like to try a different binning factor? [yes/(no)]\n";
         std::cin >> rebin_the_asymmetry;
         
      } while (rebin_the_asymmetry == "y" or rebin_the_asymmetry == "yes");
   }
   
   
   std::cout << "    Would you like to save the asymmetry? [yes/(no)]\n";
   std::string save_the_file;
   std::cin >> save_the_file;
   if (save_the_file == "y" or save_the_file == "yes")
   {
      combined_resonances.save_asymmetry(has_been_rebinned);
   }
   
   /*
   std::cout << "\n";
   std::cout << "file_name                   " << resonance.file_name << "\n";
   std::cout << "file_handle                 " << resonance.file_handle << "\n";
   std::cout << "file_type                   " << resonance.file_type << "\n";
   std::cout << "run_title                   " << resonance.run_title << "\n";
   std::cout << "run_year                    " << resonance.run_year << "\n";
   std::cout << "run_number                  " << resonance.run_number << "\n";
   std::cout << "run_experiment_number       " << resonance.run_experiment_number << "\n";
   std::cout << "run_start_time              " << std::asctime(std::localtime(&resonance.run_start_time));
   std::cout << "run_stop_time               " << std::asctime(std::localtime(&resonance.run_stop_time));
   std::cout << "run_elapse_time             " << resonance.run_elapsed_time << "\n";
   std::cout << "run_experimenters           " << resonance.run_experimenters << "\n";
   std::cout << "run_method                  " << resonance.run_method << "\n";
   std::cout << "run_mode                    " << resonance.run_mode << "\n";
   std::cout << "run_sample                  " << resonance.run_sample << "\n";
   std::cout << "run_sample_orientation      " << resonance.run_sample_orientation << "\n";
   std::cout << "run_area                    " << resonance.run_area << "\n";
   std::cout << "run_data_acquisition_system " << resonance.run_data_acquisition_system << "\n";
   std::cout << "run_lab                     " << resonance.run_lab << "\n";
   std::cout << "number_of_logged_variables  " << resonance.number_of_logged_variables << "\n";
   std::cout << "number_of_histograms        " << resonance.number_of_histograms << "\n";
   std::cout << "energy                      " << resonance.energy() << " ± " << resonance.energy_error()<< " keV\n";
   std::cout << "field                       " << resonance.field() << " ± " << resonance.field_error()<< " T\n";
   std::cout << "temperature                 " << resonance.temperature() << " ± " << resonance.temperature_error()<< " K\n";
   std::cout << "\n";
   */
   
   //std::cout << "    Close the canvas window to quit.\n";
   //root_canvas.~TCanvas();
   //root_canvas.Closed();
   
   //root_canvas.Close();
   //root_canvas.Closed();
   
   //std::cout << "Is the thread joinable? : " << std::boolalpha << root_application_thread.joinable() << "\n";
   
   
   
   
   //root_application_thread.join();
   // Is this really safe?
   root_application_thread.detach();
   root_application.Terminate();
   //std::cout << "and after?\n";
   
   return EXIT_SUCCESS;
}
