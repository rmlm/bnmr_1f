#ifndef BNMR_1F_COMBINED_H
#define BNMR_1F_COMBINED_H

#include <ctime>
#include <string>
#include <vector>

#include "bnmr_1f.h"

namespace triumf
{
   class bnmr_1f_combined 
   {
   public :
      bnmr_1f_combined(const std::vector<bnmr_1f> &runs);
      
      
      std::vector<bnmr_1f> individual_runs;
      
      // functions
      void print_info();
      double temperature();
      double temperature_error();
      double energy();
      double energy_error();
      double field();
      double field_error();
      void save_asymmetry(bool rebinned = false);
      void rebin(unsigned int binning_factor);
      
      
      /*
      void print_raw_asymmetry();
      
      // Data members
      
      // file and handles
      std::string file_name;
      int file_handle;
      unsigned int file_type;
      unsigned int number_of_logged_variables;
      unsigned int number_of_histograms;
      
      static const int strdim = 256;
      
      // headers
      unsigned int run_year;
      unsigned int run_number;
      unsigned int run_experiment_number;
      unsigned int run_elapsed_time;
      std::time_t run_start_time;
      std::time_t run_stop_time;
      std::string run_experimenters;
      std::string run_method;
      std::string run_mode; // Insert
      std::string run_title;
      std::string run_sample;
      std::string run_sample_orientation;
      std::string run_area;
      std::string run_apparatus;
      std::string run_data_acquisition_system;
      std::string run_lab;
      */
      
      // logged independent variables
      //std::vector<float> temperature_a;
      //std::vector<float> temperature_b;
      //std::vector<float> magnetic_field;
      //std::vector<float> alkali_cell_bias;
      //std::vector<float> tube_bias;
      //std::vector<float> isac_target_bias;
      //std::vector<float> platform_bias;
      //std::vector<float> laser_power;
      
      // derived quantities
      //std::vector<float> temperature;
      //std::vector<float> implantation_energy;
      
      // raw histograms
      //std::vector<unsigned int> raw_histogram_bin;
      std::vector<unsigned int> raw_histogram_frequency;
      std::vector<unsigned int> raw_histogram_B_p;
      std::vector<unsigned int> raw_histogram_B_m;
      std::vector<unsigned int> raw_histogram_F_p;
      std::vector<unsigned int> raw_histogram_F_m;
      std::vector<double> raw_histogram_asymmetry;
      std::vector<double> raw_histogram_dasymmetry;
      std::vector<unsigned int> raw_histogram_L_p;
      std::vector<unsigned int> raw_histogram_L_m;
      std::vector<unsigned int> raw_histogram_R_p;
      std::vector<unsigned int> raw_histogram_R_m;
      
      // sorted histograms
      std::vector<unsigned int> sorted_histogram_frequency;
      std::vector<double> frequency;
      
      // b-NMR Histograms
      std::vector<std::vector<unsigned int>> B_p;
      std::vector<std::vector<unsigned int>> B_m;
      std::vector<std::vector<unsigned int>> F_p;
      std::vector<std::vector<unsigned int>> F_m;
      // b-NQR Histograms matrices <-- unused, everything gets put into the "nmr" histograms for simplicity
      //std::vector<std::vector<unsigned int>> L_p;
      //std::vector<std::vector<unsigned int>> L_m;
      //std::vector<std::vector<unsigned int>> R_p;
      //std::vector<std::vector<unsigned int>> R_m;
      
      std::vector<std::vector<double>> A_p;
      std::vector<std::vector<double>> dA_p;
      std::vector<std::vector<double>> A_m;
      std::vector<std::vector<double>> dA_m;
      
      std::vector<double> asymmetry_p;
      std::vector<double> dasymmetry_p;
      
      std::vector<double> asymmetry_m;
      std::vector<double> dasymmetry_m;
      
      std::vector<double> asymmetry;
      std::vector<double> dasymmetry;
      
      // rebinned
      std::vector<double> binned_frequency;
      std::vector<double> binned_asymmetry_p;
      std::vector<double> binned_dasymmetry_p;
      std::vector<double> binned_asymmetry_m;
      std::vector<double> binned_dasymmetry_m;
      std::vector<double> binned_asymmetry;
      std::vector<double> binned_dasymmetry;
      
   protected :
   private :
      void sort_frequencies();
      void sort_histograms();
      void calculate_asymmetry_matrix_helicities();
      void calculate_average_asymmetry();
      void calculate_asymmetry_combined();
         
      //read_header();
      //read_logged_variables();
      //read_histograms();
      
   }; // class bnmr_1f_combined
} // namespace triumf
#endif // BNMR_1F_COMBINED_H
