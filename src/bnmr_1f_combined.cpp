#include <fstream>
#include <iomanip>
#include <iostream>

#include "bnmr_1f_combined.h"
#include "bnmr_asymmetry.h"
#include "bnmr_statistics.h"
#include "colour.h"

triumf::bnmr_1f_combined::bnmr_1f_combined(const std::vector<bnmr_1f> &runs)
{
   individual_runs = runs;
   // combine the run histograms
   for (auto &i: individual_runs)
   {
      raw_histogram_frequency.insert(raw_histogram_frequency.end(), i.raw_histogram_frequency.begin(), i.raw_histogram_frequency.end());
      if (i.run_number >= 40000 and i.run_number <= 44999) // bnmr
      {
         raw_histogram_B_p.insert(raw_histogram_B_p.end(), i.raw_histogram_B_p.begin(), i.raw_histogram_B_p.end());
         raw_histogram_B_m.insert(raw_histogram_B_m.end(), i.raw_histogram_B_m.begin(), i.raw_histogram_B_m.end());
         raw_histogram_F_p.insert(raw_histogram_F_p.end(), i.raw_histogram_F_p.begin(), i.raw_histogram_F_p.end());
         raw_histogram_F_m.insert(raw_histogram_F_m.end(), i.raw_histogram_F_m.begin(), i.raw_histogram_F_m.end());
      }
      if (i.run_number >= 45000 and i.run_number <= 49999)// bnqr
      {
         raw_histogram_B_p.insert(raw_histogram_B_p.end(), i.raw_histogram_L_p.begin(), i.raw_histogram_L_p.end());
         raw_histogram_B_m.insert(raw_histogram_B_m.end(), i.raw_histogram_L_m.begin(), i.raw_histogram_L_m.end());
         raw_histogram_F_p.insert(raw_histogram_F_p.end(), i.raw_histogram_R_p.begin(), i.raw_histogram_R_p.end());
         raw_histogram_F_m.insert(raw_histogram_F_m.end(), i.raw_histogram_R_m.begin(), i.raw_histogram_R_m.end());
      }
   }
     
   sort_frequencies();
   sort_histograms();
   calculate_asymmetry_matrix_helicities();
   calculate_average_asymmetry();
   
}


void triumf::bnmr_1f_combined::print_info()
{
   std::cout << "\n";
   /*
   std::cout << "    " << red << file_name << reset << "\n";
   std::cout << "    " << run_title << "\n";
   std::cout << "    " << cyan << std::asctime(std::localtime(&run_start_time)) << reset;
   std::cout << "    " << green << run_year << " " << run_number << reset;
   std::cout << "    " << run_lab << " " << run_area << reset  << " - " << run_method << " (" << run_mode << ") - " << run_data_acquisition_system << " - " <<  std::setprecision(2) << std::fixed << magenta << run_elapsed_time/60.0 << reset << " min\n";
   std::cout << "    Exp. M" << run_experiment_number << "    [" << run_experimenters << "]\n";
   std::cout << "    " << run_sample << " " << run_sample_orientation << "\n";
   */
   std::cout << "    Combined run info:\n\n";
   std::cout << std::setprecision(4) << std::fixed;
   std::cout << "    Beam Energy    = " << colour::yellow << std::setw(8) << std::right << energy() << colour::reset;
   std::cout << " ± " << colour::yellow << std::setw(7) << std::right << energy_error() << colour::reset << " keV\n";
   std::cout << "    Magnetic Field = " << colour::blue << std::setw(8) << std::right << field() << colour::reset << " ± " << colour::blue << std::setw(7) << std::right << field_error() << colour::reset << " T\n";
   std::cout << "    Temperature    = " << colour::green << std::setw(8) << std::right << temperature() << colour::reset << " ± " << colour::green << std::setw(7) << std::right << temperature_error() << colour::reset << " K\n";
   std::cout << "\n";
}


double triumf::bnmr_1f_combined::temperature()
{
   std::vector<float> temperatures_ab;
   for (auto &run: individual_runs)
   {
      temperatures_ab.insert(temperatures_ab.end(), run.temperature_a.begin(), run.temperature_a.end());
      temperatures_ab.insert(temperatures_ab.end(), run.temperature_b.begin(), run.temperature_b.end());
   }
   temperatures_ab.erase(std::remove(temperatures_ab.begin(), temperatures_ab.end(), 0), temperatures_ab.end());
   return bnmr::statistics::average(temperatures_ab);
}


double triumf::bnmr_1f_combined::temperature_error()
{
   std::vector<float> temperatures_ab;
   for (auto &run: individual_runs)
   {
      temperatures_ab.insert(temperatures_ab.end(), run.temperature_a.begin(), run.temperature_a.end());
      temperatures_ab.insert(temperatures_ab.end(), run.temperature_b.begin(), run.temperature_b.end());
   }
   temperatures_ab.erase(std::remove(temperatures_ab.begin(), temperatures_ab.end(), 0), temperatures_ab.end());
   return bnmr::statistics::standard_deviation(temperatures_ab);
}


double triumf::bnmr_1f_combined::energy()
{
   std::vector<float> targets;
   std::vector<float> alkalais;
   std::vector<float> platforms;
   for (auto &run: individual_runs)
   {
      targets.insert(targets.end(), run.isac_target_bias.begin(), run.isac_target_bias.end());
      alkalais.insert(alkalais.end(), run.alkali_cell_bias.begin(), run.alkali_cell_bias.end());
      platforms.insert(platforms.end(), run.platform_bias.begin(), run.platform_bias.end());
   }
   std::vector<double> energies;
   for (std::size_t i = 0; i < targets.size(); ++i) 
   {
      energies.push_back(targets.at(i) - alkalais.at(i) - platforms.at(i));
   }
   return bnmr::statistics::average(energies);
}


double triumf::bnmr_1f_combined::energy_error()
{
   std::vector<float> targets;
   std::vector<float> alkalais;
   std::vector<float> platforms;
   for (auto &run: individual_runs)
   {
      targets.insert(targets.end(), run.isac_target_bias.begin(), run.isac_target_bias.end());
      alkalais.insert(alkalais.end(), run.alkali_cell_bias.begin(), run.alkali_cell_bias.end());
      platforms.insert(platforms.end(), run.platform_bias.begin(), run.platform_bias.end());
   }
   std::vector<double> energies;
   for (std::size_t i = 0; i < targets.size(); ++i) 
   {
      energies.push_back(targets.at(i) - alkalais.at(i) - platforms.at(i));
   }
   return bnmr::statistics::standard_deviation(energies);
}


double triumf::bnmr_1f_combined::field()
{
   std::vector<float> fields;
   for (auto &run: individual_runs)
   {
      fields.insert(fields.end(), run.magnetic_field.begin(), run.magnetic_field.end());
   }
   return bnmr::statistics::average(fields);
}


double triumf::bnmr_1f_combined::field_error()
{
   std::vector<float> fields;
   for (auto &run: individual_runs)
   {
      fields.insert(fields.end(), run.magnetic_field.begin(), run.magnetic_field.end());
   }
   return bnmr::statistics::standard_deviation(fields);
}


void triumf::bnmr_1f_combined::sort_frequencies()
{
   // Make a temporary vector, sort it, then erase common adjacent elements
   std::vector<unsigned int> tmp_frequency(raw_histogram_frequency);
   std::sort(tmp_frequency.begin(), tmp_frequency.end());
   tmp_frequency.erase(std::unique(tmp_frequency.begin(), tmp_frequency.end()), tmp_frequency.end());
   //for (auto &f: tmp_frequency) std::cout << f << "\n";
   sorted_histogram_frequency = tmp_frequency;
   for (auto &f: sorted_histogram_frequency) frequency.push_back(f);
}

void triumf::bnmr_1f_combined::sort_histograms()
{
   // calculate the frequency-counts "matrix" for each histogram_bins
   // loop over frequencies
   for (std::size_t i = 0; i < sorted_histogram_frequency.size(); ++i)
   {
      // create some temporary vectors...
      std::vector<unsigned int> B_p_tmp;
      std::vector<unsigned int> B_m_tmp;
      std::vector<unsigned int> F_p_tmp;
      std::vector<unsigned int> F_m_tmp;
      //std::vector<unsigned int> L_p_tmp;
      //std::vector<unsigned int> L_m_tmp;
      //std::vector<unsigned int> R_p_tmp;
      //std::vector<unsigned int> R_m_tmp;
      //std::vector<unsigned int> NBMB_p_tmp;
      //std::vector<unsigned int> NBMB_m_tmp;
      //std::vector<unsigned int> NBMF_p_tmp;
      //std::vector<unsigned int> NBMF_m_tmp;
      // ...then loop over ALL points
      for (std::size_t j = 0; j < raw_histogram_frequency.size(); ++j)
      {
         if ( sorted_histogram_frequency.at(i) == raw_histogram_frequency.at(j) )
         {
            //
            // This check for non-zero counts in BOTH detector pairs fixes cases
            // out-of-range std::vector<double> issues, resultant from F/B +/-
            // pairs have DIFFERENT numbers of entries!
            //
            //if ( run_number >= 40000 and run_number < 45000 )
            //{
               /*
               if ( raw_histogram_B_p.at(j) != 0 ) B_p_tmp.push_back( raw_histogram_B_p.at(j) );
               if ( raw_histogram_B_m.at(j) != 0 ) B_m_tmp.push_back( raw_histogram_B_m.at(j) );
               if ( raw_histogram_F_p.at(j) != 0 ) F_p_tmp.push_back( raw_histogram_F_p.at(j) );
               if ( raw_histogram_F_m.at(j) != 0 ) F_m_tmp.push_back( raw_histogram_F_m.at(j) );
               */
               if ( raw_histogram_B_p.at(j) != 0 and raw_histogram_F_p.at(j) != 0  ) { B_p_tmp.push_back( raw_histogram_B_p.at(j) ); F_p_tmp.push_back( raw_histogram_F_p.at(j) ); }
               if ( raw_histogram_B_m.at(j) != 0 and raw_histogram_F_m.at(j) != 0  ) { B_m_tmp.push_back( raw_histogram_B_m.at(j) ); F_m_tmp.push_back( raw_histogram_F_m.at(j) ); }
            //}
            //if ( run_number >= 45000 and run_number < 50000 )
            //{
               /*
               if ( raw_histogram_L_p.at(j) != 0 ) L_p_tmp.push_back( raw_histogram_L_p.at(j) );
               if ( raw_histogram_L_m.at(j) != 0 ) L_m_tmp.push_back( raw_histogram_L_m.at(j) );
               if ( raw_histogram_R_p.at(j) != 0 ) R_p_tmp.push_back( raw_histogram_R_p.at(j) );
               if ( raw_histogram_R_m.at(j) != 0 ) R_m_tmp.push_back( raw_histogram_R_m.at(j) );
               */
               //if ( raw_histogram_L_p.at(j) != 0 and raw_histogram_R_p.at(j) != 0  ) { L_p_tmp.push_back( raw_histogram_L_p.at(j) ); R_p_tmp.push_back( raw_histogram_R_p.at(j) ); }
               //if ( raw_histogram_L_m.at(j) != 0 and raw_histogram_R_m.at(j) != 0  ) { L_m_tmp.push_back( raw_histogram_L_m.at(j) ); R_m_tmp.push_back( raw_histogram_R_m.at(j) ); }
            //}
            /*
            if ( vNBMB_p.at(j) != 0 ) NBMB_p_tmp.push_back( vNBMB_p.at(j) );
            if ( vNBMB_m.at(j) != 0 ) NBMB_m_tmp.push_back( vNBMB_m.at(j) );
            if ( vNBMF_p.at(j) != 0 ) NBMF_p_tmp.push_back( vNBMF_p.at(j) );
            if ( vNBMF_m.at(j) != 0 ) NBMF_m_tmp.push_back( vNBMF_m.at(j) );
            */
            //if ( vNBMB_p.at(j) != 0 and vNBMF_p.at(j) != 0  ) { NBMB_p_tmp.push_back( vNBMB_p.at(j) ); NBMF_p_tmp.push_back( vNBMF_p.at(j) ); }
            //if ( vNBMB_m.at(j) != 0 and vNBMF_m.at(j) != 0  ) { NBMB_m_tmp.push_back( vNBMB_m.at(j) ); NBMF_m_tmp.push_back( vNBMF_m.at(j) ); }
         }
      }
      // push the tmp vectors into the histogram matrix
      B_p.push_back( B_p_tmp ); B_p_tmp.clear();
      B_m.push_back( B_m_tmp ); B_m_tmp.clear();
      F_p.push_back( F_p_tmp ); F_p_tmp.clear();
      F_m.push_back( F_m_tmp ); F_m_tmp.clear();
      //L_p.push_back( L_p_tmp ); L_p_tmp.clear();
      //L_m.push_back( L_m_tmp ); L_m_tmp.clear();
      //R_p.push_back( R_p_tmp ); R_p_tmp.clear();
      //R_m.push_back( R_m_tmp ); R_m_tmp.clear();
      //NBMB_p.push_back( NBMB_p_tmp ); NBMB_p_tmp.clear();
      //NBMB_m.push_back( NBMB_m_tmp ); NBMB_m_tmp.clear();
      //NBMF_p.push_back( NBMF_p_tmp ); NBMF_p_tmp.clear();
      //NBMF_m.push_back( NBMF_m_tmp ); NBMF_m_tmp.clear();
   } 
}

void triumf::bnmr_1f_combined::calculate_asymmetry_matrix_helicities()
{
   for (std::size_t i = 0; i < sorted_histogram_frequency.size(); ++i )
   {
      std::vector<double> A_p_tmp;
      std::vector<double> dA_p_tmp;
      std::vector<double> A_m_tmp;
      std::vector<double> dA_m_tmp;
      std::vector<double> NBMA_p_tmp;
      std::vector<double> dNBMA_p_tmp;
      std::vector<double> NBMA_m_tmp;
      std::vector<double> dNBMA_m_tmp;
      //
      //if ( run_number >= 40000 and run_number < 45000 ) 
      //{
         for (std::size_t j = 0; j < B_p.at(i).size(); ++j )
         {
            A_p_tmp.push_back(bnmr::asymmetry::calculate(B_p.at(i).at(j),F_p.at(i).at(j)) );
            dA_p_tmp.push_back(bnmr::asymmetry::calculate_uncertainty(B_p.at(i).at(j),F_p.at(i).at(j)) );
            //A_p_tmp.push_back( ( static_cast<double>(B_p.at(i).at(j))-F_p.at(i).at(j))/(B_p.at(i).at(j)+F_p.at(i).at(j)) );
            //dA_p_tmp.push_back( 2*std::sqrt( static_cast<double>(B_p.at(i).at(j))*F_p.at(i).at(j) )/std::pow( B_p.at(i).at(j) + F_p.at(i).at(j) ,1.5) );
            //NBMA_p_tmp.push_back( ( static_cast<double>(NBMB_p.at(i).at(j))-NBMF_p.at(i).at(j))/(NBMB_p.at(i).at(j)+NBMF_p.at(i).at(j)) );
            //dNBMA_p_tmp.push_back( 2*std::sqrt( static_cast<double>(NBMB_p.at(i).at(j))*NBMF_p.at(i).at(j) )/std::pow( NBMB_p.at(i).at(j) + NBMF_p.at(i).at(j) ,1.5) );
         }
      //}
      /*
      if ( run_number >= 45000 and run_number < 50000 ) 
      {
         for (std::size_t j = 0; j < L_p.at(i).size(); ++j )
         {
            A_p_tmp.push_back( ( static_cast<double>(L_p.at(i).at(j))-R_p.at(i).at(j))/(L_p.at(i).at(j)+R_p.at(i).at(j)) );
            dA_p_tmp.push_back( 2*std::sqrt( static_cast<double>(L_p.at(i).at(j))*R_p.at(i).at(j) )/std::pow( L_p.at(i).at(j) + L_p.at(i).at(j) ,1.5) );
            NBMA_p_tmp.push_back( ( static_cast<double>(NBMB_p.at(i).at(j))-NBMF_p.at(i).at(j))/(NBMB_p.at(i).at(j)+NBMF_p.at(i).at(j)) );
            dNBMA_p_tmp.push_back( 2*std::sqrt( static_cast<double>(NBMB_p.at(i).at(j))*NBMF_p.at(i).at(j) )/std::pow( NBMB_p.at(i).at(j) + NBMF_p.at(i).at(j) ,1.5) );
         }
      }
      */
      A_p.push_back( A_p_tmp );
      A_p_tmp.clear();
      dA_p.push_back( dA_p_tmp );
      dA_p_tmp.clear();
      //NBMA_p.push_back( NBMA_p_tmp );
      //NBMA_p_tmp.clear();
      //dNBMA_p.push_back( dNBMA_p_tmp );
      //dNBMA_p_tmp.clear();
      
      //if ( run_number >= 40000 and run_number < 45000 ) 
      //{
         for (std::size_t j = 0; j < B_m.at(i).size(); ++j )
         {
            A_m_tmp.push_back(bnmr::asymmetry::calculate(B_m.at(i).at(j),F_m.at(i).at(j)) );
            dA_m_tmp.push_back(bnmr::asymmetry::calculate_uncertainty(B_m.at(i).at(j),F_m.at(i).at(j)) );
            //A_m_tmp.push_back( ( static_cast<double>(B_m.at(i).at(j))-F_m.at(i).at(j))/(B_m.at(i).at(j)+F_m.at(i).at(j)) );
            //dA_m_tmp.push_back( 2*std::sqrt( static_cast<double>(B_m.at(i).at(j))*F_m.at(i).at(j) )/std::pow( B_m.at(i).at(j) + F_m.at(i).at(j) ,1.5) );
            //NBMA_m_tmp.push_back( ( static_cast<double>(NBMB_m.at(i).at(j))-NBMF_m.at(i).at(j))/(NBMB_m.at(i).at(j)+NBMF_m.at(i).at(j)) );
            //dNBMA_m_tmp.push_back( 2*std::sqrt( static_cast<double>(NBMB_m.at(i).at(j))*NBMF_m.at(i).at(j) )/std::pow( NBMB_m.at(i).at(j) + NBMF_m.at(i).at(j) ,1.5) );
         }
      //}
      
      /*
      if ( run_number >= 45000 and run_number < 50000 )
      {
         for (std::size_t j = 0; j < L_m.at(i).size(); ++j )
         {
            A_m_tmp.push_back( ( static_cast<double>(L_m.at(i).at(j))-R_m.at(i).at(j))/(L_m.at(i).at(j)+R_m.at(i).at(j)) );
            dA_m_tmp.push_back( 2*std::sqrt( static_cast<double>(L_m.at(i).at(j))*R_m.at(i).at(j) )/std::pow( L_m.at(i).at(j) + L_m.at(i).at(j) ,1.5) );
            NBMA_m_tmp.push_back( ( static_cast<double>(NBMB_m.at(i).at(j))-NBMF_m.at(i).at(j))/(NBMB_m.at(i).at(j)+NBMF_m.at(i).at(j)) );
            dNBMA_m_tmp.push_back( 2*std::sqrt( static_cast<double>(NBMB_m.at(i).at(j))*NBMF_m.at(i).at(j) )/std::pow( NBMB_m.at(i).at(j) + NBMF_m.at(i).at(j) ,1.5) );
         }
      }
      */
      
      A_m.push_back( A_m_tmp );
      A_m_tmp.clear();
      dA_m.push_back( dA_m_tmp );
      dA_m_tmp.clear();
      //NBMA_m.push_back( NBMA_m_tmp );
      //NBMA_m_tmp.clear();
      //dNBMA_m.push_back( dNBMA_m_tmp );
      //dNBMA_m_tmp.clear();
   }
}

void triumf::bnmr_1f_combined::calculate_average_asymmetry()
{
   // compute A averages
   for (std::size_t i = 0; i < sorted_histogram_frequency.size(); ++i )
   {
      asymmetry_p.push_back(bnmr::statistics::weighted_average(A_p.at(i), dA_p.at(i)));
      dasymmetry_p.push_back(bnmr::statistics::weighted_standard_deviation(dA_p.at(i)));
      asymmetry_m.push_back(bnmr::statistics::weighted_average(A_m.at(i), dA_m.at(i)));
      dasymmetry_m.push_back(bnmr::statistics::weighted_standard_deviation(dA_m.at(i)));
      asymmetry.push_back(0.5*(asymmetry_p.at(i) - asymmetry_m.at(i)));
      dasymmetry.push_back(0.5*std::sqrt(dasymmetry_p.at(i)*dasymmetry_p.at(i) + dasymmetry_m.at(i)*dasymmetry_m.at(i)));
   }
}


void triumf::bnmr_1f_combined::save_asymmetry(bool rebinned)
{
   std::string outputfilename("combined");
   for (auto &r: individual_runs)
   {
      outputfilename.append("-" + std::to_string(r.run_year) + "-" + std::to_string(r.run_number));
   }
   outputfilename.append(".dat");
   std::ofstream outputfile;
   outputfile.open( outputfilename.c_str() );
   // 
   outputfile << "# Combined 1f runs (Year-RunNumber):\n";
   outputfile << "# ";
   for (auto &r: individual_runs) outputfile << std::to_string(r.run_year) << "-" << std::to_string(r.run_number) << "   ";
   outputfile << "\n";
   outputfile << "#\n";
   outputfile << std::setprecision(4) << std::fixed;
   outputfile << "# Energy = " << energy() << " +/- " << energy_error() << " keV\n";
   outputfile << "# Field = " << field() << " +/- " << field_error() << " T\n";
   outputfile << "# Temperature = " << temperature() << " +/- " << temperature_error() << " K\n";
   outputfile << "#\n";
   outputfile << "#Frequency(Hz)\tA+(a.u.)\tdA+(a.u.)\tA-(a.u.)\tdA-(a.u.)\tA(a.u.)\tdA(a.u.)\n";
   if (rebinned == true)
   {
      for (std::size_t i = 0; i < binned_frequency.size(); ++i)
      {
          outputfile << std::setprecision(3) << std::fixed;
          outputfile << binned_frequency.at(i) << "\t";
          outputfile << std::setprecision(9) << std::fixed;
          outputfile << binned_asymmetry_p.at(i) << "\t" << binned_dasymmetry_p.at(i) << "\t";
          outputfile << binned_asymmetry_m.at(i) << "\t" << binned_dasymmetry_m.at(i) << "\t";
          outputfile << binned_asymmetry.at(i) << "\t" << binned_dasymmetry.at(i) << "\n";
      }
   }
   else
   {
      for (std::size_t i = 0; i < sorted_histogram_frequency.size(); ++i)
      {
          outputfile << std::setprecision(0) << std::fixed;
          outputfile << sorted_histogram_frequency.at(i) << "\t";
          outputfile << std::setprecision(9) << std::fixed;
          outputfile << asymmetry_p.at(i) << "\t" << dasymmetry_p.at(i) << "\t";
          outputfile << asymmetry_m.at(i) << "\t" << dasymmetry_m.at(i) << "\t";
          outputfile << asymmetry.at(i) << "\t" << dasymmetry.at(i) << "\n";
      }
   }
   outputfile.close();
}


void triumf::bnmr_1f_combined::rebin(unsigned int binning_factor)
{
   // clear the actual binned vectors
   binned_frequency.clear();
   binned_asymmetry_p.clear();
   binned_dasymmetry_p.clear();
   binned_asymmetry_m.clear();
   binned_dasymmetry_m.clear();
   binned_asymmetry.clear();
   binned_dasymmetry.clear();
   // create some temporary vectors
   std::vector<double> tmp_frequency;
   std::vector<double> tmp_asymmetry_p;
   std::vector<double> tmp_dasymmetry_p;
   std::vector<double> tmp_asymmetry_m;
   std::vector<double> tmp_dasymmetry_m;
   std::vector<double> tmp_asymmetry;
   std::vector<double> tmp_dasymmetry;
   unsigned int counter = 0;
   // loop over all 
   for (std::size_t i = 0; i < frequency.size(); ++i)
   {
      // push_back the temporaries...
      tmp_frequency.push_back(frequency.at(i));
      tmp_asymmetry_p.push_back(asymmetry_p.at(i));
      tmp_dasymmetry_p.push_back(dasymmetry_p.at(i));
      tmp_asymmetry_m.push_back(asymmetry_m.at(i));
      tmp_dasymmetry_m.push_back(dasymmetry_m.at(i));
      tmp_asymmetry.push_back(asymmetry.at(i));
      tmp_dasymmetry.push_back(dasymmetry.at(i));
      // ...increment the counter...
      counter += 1;
      // ...and combine the bins after the appropriate number of steps.
      if (counter == binning_factor)
      {
         // rebin the frequencies
         binned_frequency.push_back(bnmr::statistics::average(tmp_frequency));
         // rebin the histograms
         binned_asymmetry_p.push_back(bnmr::statistics::weighted_average(tmp_asymmetry_p, tmp_dasymmetry_p));
         binned_dasymmetry_p.push_back(bnmr::statistics::weighted_standard_deviation(tmp_dasymmetry_p));
         binned_asymmetry_m.push_back(bnmr::statistics::weighted_average(tmp_asymmetry_m, tmp_dasymmetry_m));
         binned_dasymmetry_m.push_back(bnmr::statistics::weighted_standard_deviation(tmp_dasymmetry_m));
         binned_asymmetry.push_back(bnmr::statistics::weighted_average(tmp_asymmetry, tmp_dasymmetry));
         binned_dasymmetry.push_back(bnmr::statistics::weighted_standard_deviation(tmp_dasymmetry));
         // clear the temporary vectors
         tmp_frequency.clear();
         tmp_asymmetry_p.clear();
         tmp_dasymmetry_p.clear();
         tmp_asymmetry_m.clear();
         tmp_dasymmetry_m.clear();
         tmp_asymmetry.clear();
         tmp_dasymmetry.clear();
         // reset the counter
         counter = 0;
      }
   }
}
