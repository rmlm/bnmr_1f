#ifndef BNMR_1F_H
#define BNMR_1F_H

#include <ctime>
#include <string>
#include <vector>

//#include "bnmr.h"

namespace triumf
{
   class bnmr_1f //: public bnmr
   {
   public :
      bnmr_1f(unsigned int year, unsigned int run_number);
      
      // functions
      void print_info();
      double temperature();
      double temperature_error();
      double energy();
      double energy_error();
      double field();
      double field_error();
      void print_raw_asymmetry();
      void save_asymmetry();
      void remove_bad_bins();
      
      // Data members
      
      // file and handles
      std::string file_name;
      int file_handle;
      unsigned int file_type;
      unsigned int number_of_logged_variables;
      unsigned int number_of_histograms;
      
      static const int strdim = 256;
      
      // headers
      unsigned int run_year;
      unsigned int run_number;
      unsigned int run_experiment_number;
      unsigned int run_elapsed_time;
      std::time_t run_start_time;
      std::time_t run_stop_time;
      std::string run_experimenters;
      std::string run_method;
      std::string run_mode; // Insert
      std::string run_title;
      std::string run_sample;
      std::string run_sample_orientation;
      std::string run_area;
      std::string run_apparatus;
      std::string run_data_acquisition_system;
      std::string run_lab;
      
      // logged independent variables
      std::vector<float> temperature_a;
      std::vector<float> temperature_b;
      std::vector<float> magnetic_field;
      std::vector<float> helmholtz_coil_current;
      std::vector<float> alkali_cell_bias;
      std::vector<float> tube_bias;
      std::vector<float> isac_target_bias;
      std::vector<float> platform_bias;
      std::vector<float> laser_power;
      
      // raw histograms
      std::vector<double> raw_histogram_bin;
      std::vector<unsigned int> raw_histogram_frequency;
      std::vector<unsigned int> raw_histogram_B_p;
      std::vector<unsigned int> raw_histogram_B_m;
      std::vector<unsigned int> raw_histogram_F_p;
      std::vector<unsigned int> raw_histogram_F_m;
      std::vector<double> raw_histogram_asymmetry;
      std::vector<double> raw_histogram_dasymmetry;
      std::vector<unsigned int> raw_histogram_L_p;
      std::vector<unsigned int> raw_histogram_L_m;
      std::vector<unsigned int> raw_histogram_R_p;
      std::vector<unsigned int> raw_histogram_R_m;
      
      // sorted histograms
      std::vector<unsigned int> sorted_histogram_frequency;
      
      // b-NMR Histograms matrices
      std::vector<std::vector<unsigned int>> B_p;
      std::vector<std::vector<unsigned int>> B_m;
      std::vector<std::vector<unsigned int>> F_p;
      std::vector<std::vector<unsigned int>> F_m;
      // b-NQR Histograms matrices
      std::vector<std::vector<unsigned int>> L_p;
      std::vector<std::vector<unsigned int>> L_m;
      std::vector<std::vector<unsigned int>> R_p;
      std::vector<std::vector<unsigned int>> R_m;
      
      // asymmetry matrices
      std::vector<std::vector<double>> A_p;
      std::vector<std::vector<double>> dA_p;
      std::vector<std::vector<double>> A_m;
      std::vector<std::vector<double>> dA_m;
      
      // averaged/combined asymmetries
      std::vector<double> asymmetry_p;
      std::vector<double> dasymmetry_p;
      std::vector<double> asymmetry_m;
      std::vector<double> dasymmetry_m;
      std::vector<double> asymmetry;
      std::vector<double> dasymmetry;
      
   protected :
   private :
      void sort_frequencies();
      void sort_histograms();
      void calculate_asymmetry_matrix_helicities();
      void calculate_average_asymmetry();
      //void calculate_asymmetry_combined();
      //read_header();
      //read_logged_variables();
      //read_histograms();
      
   }; // class bnmr_1f
} // namespace triumf
#endif // BNMR_1F_H
