#ifndef COLOUR_H
#define COLOUR_H

#include <string>

namespace colour
{
   extern const std::string black;
   extern const std::string red;
   extern const std::string green;
   extern const std::string yellow;
   extern const std::string blue;
   extern const std::string magenta;
   extern const std::string cyan;
   extern const std::string white;
   extern const std::string reset;
} // namespace colour
#endif // COLOUR_H
