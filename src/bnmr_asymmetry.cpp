#include "bnmr_asymmetry.h"

#include <cmath>

// calculates asymmetry & uncertainty for the usual two-counter method
double bnmr::asymmetry::calculate(unsigned int backward_counts, unsigned int forward_counts)
{
   return (static_cast<double>(backward_counts)-static_cast<double>(forward_counts))/(static_cast<double>(backward_counts)+static_cast<double>(forward_counts));
}

double bnmr::asymmetry::calculate_uncertainty(unsigned int backward_counts, unsigned int forward_counts)
{
   return 2.0*std::sqrt(static_cast<double>(backward_counts)*static_cast<double>(forward_counts))/std::pow(static_cast<double>(backward_counts)+static_cast<double>(forward_counts),1.5);
}


// calculates the asymmetry for the four-counter method (lcrplot style)
double bnmr::asymmetry::calculate(unsigned int backward_p_counts, unsigned int forward_p_counts, unsigned int backward_m_counts, unsigned int forward_m_counts)
{
   return (static_cast<double>(backward_p_counts)-static_cast<double>(forward_p_counts)+static_cast<double>(backward_m_counts)-static_cast<double>(forward_m_counts))/(static_cast<double>(backward_p_counts)+static_cast<double>(forward_p_counts)+static_cast<double>(backward_m_counts)+static_cast<double>(forward_m_counts));
}

double bnmr::asymmetry::calculate_uncertainty(unsigned int backward_p_counts, unsigned int forward_p_counts, unsigned int backward_m_counts, unsigned int forward_m_counts)
{
   return 2.0*std::sqrt((static_cast<double>(backward_p_counts)+static_cast<double>(backward_m_counts))*(static_cast<double>(forward_p_counts)+static_cast<double>(forward_m_counts)))/std::pow(static_cast<double>(backward_p_counts)+static_cast<double>(forward_p_counts)+static_cast<double>(backward_m_counts)+static_cast<double>(forward_m_counts),1.5);
}

